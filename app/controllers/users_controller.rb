#encoding: utf-8
class UsersController < ApplicationController
  before_filter :subscriptions_counting

  def new
    @user = User.new
    @user.build_subscription if @user.subscription.blank?
  end

  def create
    @user = User.new(params[:user])

    if @user.save
      redirect_to(root_path, :notice => "Ficha de inscrição cadastrada com sucesso!")
    else
      flash[:error] = "NÃO FOI POSSÍVEL COMPLETAR SEU CADASTRO, FAVOR VERIFICAR SE OS DADOS ESTÃO PREENCHIDOS CORRETAMENTE."
      render :action => "new"
    end
  end

end
