class ApplicationController < ActionController::Base

  before_filter :set_locale
  layout :layout_by_resource

  protect_from_forgery

  def subscriptions_counting
    @subscriptions_counting = Subscription.count
    @orientations = Orientation.joins(:admin).where("admins.superuser = 1").order("orientations.created_at DESC")
  end

  private

  def set_locale
    I18n.locale = params[:locale] if params.include?('locale')
  end

  def default_url_options(options={})
    options.merge!({ :locale => I18n.locale })
  end

  protected

  def layout_by_resource
    if devise_controller? && resource_name == :admin
      "admin"
    else
      "application"
    end
  end
end
