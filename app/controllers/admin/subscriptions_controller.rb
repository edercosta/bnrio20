#encoding: utf-8

class Admin::SubscriptionsController < ApplicationController

  before_filter :admin_superuser
  before_filter :authenticate_admin!
  
  layout "admin"

  def index
    @subscriptions = Subscription.all
  end

  def show
    @subscription = Subscription.find(params[:id])
  end

  private

  def admin_superuser
    if !current_admin.superuser?
      redirect_to(admin_root_path, :notice => "Você não tem permissão para acessar esta página.")
    end
  end
end
