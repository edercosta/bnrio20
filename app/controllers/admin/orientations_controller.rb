#encoding: utf-8
class Admin::OrientationsController < ApplicationController

  before_filter :orientation, :only => [:admin_superuser, :destroy, :edit, :show, :update]
  before_filter :admin_superuser, :only => [:edit, :show] 
  before_filter :authenticate_admin!

  layout "admin"

  def index
    if current_admin.superuser?
      @orientations = Orientation.order("created_at DESC")
    else
      @orientations = current_admin.orientations.order("created_at DESC")
    end
  end

  def show
  end

  def new
    @orientation = Orientation.new
  end

  def edit
  end

  def create
    @orientation = Orientation.new(params[:orientation])
    @orientation.admin = current_admin

    if @orientation.save
      redirect_to admin_orientations_path, notice: 'Orientação criada com sucesso!' 
    else
      render action: "new" 
    end
  end

  def update
    if @orientation.update_attributes(params[:orientation])
      redirect_to admin_orientations_path, notice: 'Orientação atualizada com sucesso!' 
    else
      render action: "edit" 
    end
  end

  def destroy
    @orientation.destroy

    redirect_to admin_orientations_path
  end

  private

  def orientation
    @orientation = Orientation.find(params[:id])
  end

  def admin_superuser
    if !current_admin.superuser? && (@orientation.admin.id != current_admin.id)
      redirect_to(admin_root_path, :notice => "Você não tem permissão para acessar esta página.")
    end
  end
end
