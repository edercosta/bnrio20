# encoding: utf-8

class Admin::AdminsController < ApplicationController

  layout 'admin'

  before_filter :admin_superuser
  before_filter :authenticate_admin!
  before_filter :admin, :only => [:edit, :update, :destroy]

  def index
    @admins = Admin.all
  end

  def new
    @admin = Admin.new
  end

  def create
    @admin = Admin.new(params[:admin])

    if @admin.save
      redirect_to(admin_admins_url, :notice => 'Usuário cadastrado com sucesso!')
    else
      render :action => "new"
    end
  end

  def edit
  end

  def update
    if @admin.update_attributes(params[:admin])
      redirect_to(admin_admins_url, :notice => 'Usuário atualizado com sucesso!')
    else
      render :action => "edit"
    end
  end

  def destroy
    @admin.destroy
    redirect_to(admin_admins_url, :notice => 'Usuário excluído com sucesso!')
  end

  private

  def admin
    @admin = Admin.find(params[:id])
  end

  def admin_superuser
    if !current_admin.superuser? && (params[:id] != current_admin.id.to_s)
      redirect_to(admin_root_path, :notice => "Você não tem permissão para acessar esta página.")
    end
  end
end
