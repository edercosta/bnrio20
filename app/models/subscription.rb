#encoding: utf-8
class Subscription < ActiveRecord::Base

  validates :accept, :inclusion => { :in => [true], :message => "blabla" } 

  attr_accessible :name, :birth_day, :birth_place, :address, :district, :city,
                  :state, :contact_phone, :blood_type, :allergy, :emergency_name,
                  :emergency_address, :emergency_contact_phone, :rio20, :cupula_dos_povos,
                  :accept

  def rio20_humanize
    rio20? ? "Sim" : "Não"
  end

  def cupula_dos_povos_humanize
    cupula_dos_povos? ? "Sim" : "Não"
  end

  #Relationships
  belongs_to :user

end
