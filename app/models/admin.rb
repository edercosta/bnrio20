class Admin < ActiveRecord::Base

  validates :name, :state, :presence => true
  validates :superuser, :inclusion => [true, false]

  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :registerable
  attr_accessible :email, :password, :password_confirmation, :remember_me, :name, :state, :superuser

  has_many :orientations
end
