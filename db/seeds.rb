# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Admin.find_or_create_by_email(:email => "admin@user.com", :password => "nbrio", :password_confirmation => "nbrio", :name => "admin", :state => "nenhum", :superuser => true)
