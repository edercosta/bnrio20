class AddUserIdOnSubscriptions < ActiveRecord::Migration
  def up
    add_column :subscriptions, :user_id, :integer
  end

  def down
    remove_column :subscriptions, :user_id
  end
end
