class RemoveColumnsFromSubscription < ActiveRecord::Migration
  def up
    remove_column :subscriptions, :email
    remove_column :subscriptions, :cpf
    remove_column :subscriptions, :rg
  end

  def down
    add_column :subscriptions, :email, :string
    add_column :subscriptions, :cpf, :string
    add_column :subscriptions, :string
  end
end
