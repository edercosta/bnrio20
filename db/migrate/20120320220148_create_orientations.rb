class CreateOrientations < ActiveRecord::Migration
  def change
    create_table :orientations do |t|
      t.string :title
      t.text :description
      t.references :admin

      t.timestamps
    end
    add_index :orientations, :admin_id
  end
end
