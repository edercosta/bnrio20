class AddResponsibleDataToAdmin < ActiveRecord::Migration
  def change
    add_column :admins, :name, :string
    add_column :admins, :state, :string
    add_column :admins, :superuser, :boolean
  end
end
