class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :name
      t.string :rg
      t.string :cpf
      t.string :passport
      t.date :birth_day
      t.string :birth_place
      t.string :address
      t.string :district
      t.string :city
      t.string :state
      t.string :contact_phone
      t.string :email
      t.string :blood_type
      t.text :allergy
      t.string :emergency_name
      t.string :emergency_address
      t.string :emergency_contact_phone
      t.boolean :rio20
      t.boolean :cupula_dos_povos

      t.timestamps
    end
  end
end
