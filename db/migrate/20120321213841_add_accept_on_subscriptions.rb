class AddAcceptOnSubscriptions < ActiveRecord::Migration
  def up
    add_column :subscriptions, :accept, :boolean
  end

  def down
    remove_column :subscriptions, :accept
  end
end
