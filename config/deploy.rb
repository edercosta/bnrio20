require 'bundler/capistrano'

set :domain, "97.107.128.128"
set :application, "bicicletada"
set :repository,  "git@bitbucket.org:zigotto/bicicletada.git"

set :user, "zigotto"
set :use_sudo, false
set :deploy_to, "/home/#{user}/rails_app/#{application}"
set :scm, :git
set :keep_releases, 3
set :branch, "master"

server domain, :app, :web, :db, :primary => true

namespace :deploy do
  task :start do ; end
  task :stop do ; end

  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end

  task :before_symlink do
    run "cd #{release_path} && rake db:migrate RAILS_ENV=production"
  end
end
