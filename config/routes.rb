Bicicletada::Application.routes.draw do

  # login
  devise_for :admins

  # /admin
  namespace :admin do
    root :to => 'dashboard#index'
    resources :subscriptions, :only => [:index, :show]
    resources :admins
    resources :orientations
  end

  scope "(:locale)", :locale => /pt-BR|en/ do
    root :to => 'home#index'
    resources :users
    resource :bicicletada_nacional_rio_20, :as => :about, :controller => :about, :only => [:show]
    resource :rotas_bicicletada_nacional, :as => :routes, :controller => :routes, :only => [:show]
    resource :organizadores, :as => :organizers, :controller => :organizers, :only => [:show]
    resource :apoio, :as => :support, :controller => :support, :only => [:show]
    resource :contato, :as => :contact, :controller => :contact, :only => [:show]
    resources :orientacoes, :as => :orientations, :controller => :orientations, :only => [:index]
  end
end
