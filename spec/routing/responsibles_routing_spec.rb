require "spec_helper"

describe ResponsiblesController do
  describe "routing" do

    it "routes to #index" do
      get("/responsibles").should route_to("responsibles#index")
    end

    it "routes to #new" do
      get("/responsibles/new").should route_to("responsibles#new")
    end

    it "routes to #show" do
      get("/responsibles/1").should route_to("responsibles#show", :id => "1")
    end

    it "routes to #edit" do
      get("/responsibles/1/edit").should route_to("responsibles#edit", :id => "1")
    end

    it "routes to #create" do
      post("/responsibles").should route_to("responsibles#create")
    end

    it "routes to #update" do
      put("/responsibles/1").should route_to("responsibles#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/responsibles/1").should route_to("responsibles#destroy", :id => "1")
    end

  end
end
